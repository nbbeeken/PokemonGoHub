package hud;

import hud.gui.PkmnView;
import hud.network.Requisitions;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import resources.Res;

public class Main extends Application {

    private Pane root;

    @Override
    public void start(Stage primaryStage) throws Exception{
        createParent();
        Controller control = new Controller(getRoot(), primaryStage);
        Requisitions req = new Requisitions();
        Scene scene = new Scene(getRoot());

        BackgroundImage bgImage = new BackgroundImage(
                new Image(Res.getRes("flat_openpdx.png")),
                BackgroundRepeat.REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT
        );
        getRoot().setBackground(new Background(bgImage));
        PkmnView pkmn = new PkmnView(new Image(Res.getRes("MissingNo.png")), "Missing No");


        req.getandSetPokemonInfo(pkmn, 25);

        scene.setOnKeyPressed(control::handleKeyPress);


        ImageView quit = new ImageView(new Image(Res.getRes("close_red.png")));
        quit.setFitWidth(12);
        quit.setFitHeight(12);
        quit.setX(130);
        quit.setY(20);

        quit.setOnMouseClicked(control::quit);

        ImageView minimize = new ImageView(new Image(Res.getRes("minimize_yellow.png")));
        minimize.setFitWidth(12);
        minimize.setFitHeight(12);
        minimize.setX(156);
        minimize.setY(19.5);


        TextField entry = new TextField();
        entry.setPromptText("Enter Pkmn #");
        entry.setLayoutX(413);
        entry.setLayoutY(175);
        entry.setBackground(null);
        entry.setStyle(""
                + "-fx-font-size: 20px;"
                + "-fx-font-style: italic;"
                + "-fx-font-weight: bold;"
                + "-fx-text-fill: white;");

        entry.textProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.equals("")){
                return;
            }
            if (!newValue.matches("\\d*")) {
                entry.setText(newValue.replaceAll("[^\\d]", ""));
            }
            int pkdxnum = Integer.parseInt(newValue);
            if(pkdxnum > 721){
                entry.setText("721");
            }
        });
        entry.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER)){
                req.getandSetPokemonInfo(pkmn, Integer.parseInt(entry.getText()));
            }
        });


        String f = Res.getResPath("opening.mp3");
        Media hit = new Media(f);
        MediaPlayer mediaPlayer = new MediaPlayer(hit);
        mediaPlayer.setAutoPlay(true);
        mediaPlayer.setOnEndOfMedia(() -> mediaPlayer.seek(Duration.ZERO));

        MediaView mediaView = new MediaView(mediaPlayer);

        ImageView mediaIcon = new ImageView(new Image(Res.getRes("pause_yellow.png")));
        mediaIcon.setFitHeight(25);
        mediaIcon.setFitWidth(25);
        Button pause = new Button("", mediaIcon);
        pause.setLayoutX(558);
        pause.setLayoutY(347);
        pause.setBackground(null);

        pause.setOnMouseClicked(event -> {
            if(mediaPlayer.getStatus() == MediaPlayer.Status.PAUSED){
                mediaPlayer.play();
                mediaIcon.setImage(new Image(Res.getRes("pause_yellow.png")));
            }else {
                mediaPlayer.pause();
                mediaIcon.setImage(new Image(Res.getRes("play.png")));
            }
        });

        //ImageView test = new ImageView(new Image("http://cdn.bulbagarden.net/upload/thumb/0/0d/025Pikachu.png/250px-025Pikachu.png"));

        minimize.setOnMouseClicked(control::minimize);

        getRoot().setOnMousePressed(control::moveWindowPressed);
        getRoot().setOnMouseDragged(control::moveWindowDragged);

        getRoot().getChildren().addAll(quit, minimize, pkmn.node(), entry, mediaView, pause);

        primaryStage.setTitle("GO HUD");
        scene.setFill(null);
        primaryStage.setScene(scene);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.show();
    }

    private void createParent() {
        root = new Pane();
        root.setPrefSize(649, 502);
    }

    private Pane getRoot() {
        return root;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
