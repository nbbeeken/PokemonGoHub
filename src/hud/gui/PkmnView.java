package hud.gui;

import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * @author Neal Beeken on Jul 18, 2016.
 */
public class PkmnView{

    private ImageView view;
    private Text nameLabel;

    public PkmnView(Image image, String name) {
        view = new ImageView(image);
        nameLabel = new Text(name);
//        view.setX(81);
//        view.setY(155);
        view.setFitWidth(130);
        view.setFitHeight(130);
        nameLabel.setFont(Font.font(16));
    }

    public ImageView getView() {
        return view;
    }

    public void setView(ImageView view) {
        this.view = view;
    }

    public void setImage(Image image) {
        view.setImage(image);
    }


    public Node node(){
        VBox vBox = new VBox(view, nameLabel);
        vBox.setLayoutX(100);
        vBox.setLayoutY(180);
        return vBox;
    }

    public void setName(String name) {
        this.nameLabel.setText(name.substring(0, 1).toUpperCase() + name.substring(1));
    }
}
