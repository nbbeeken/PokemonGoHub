package hud.network;

import hud.gui.PkmnView;
import hud.model.PokemonForm;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author Neal Beeken on Jul 18, 2016.
 */
public class Requisitions {

    private int current;

    public void getandSetPokemonInfo(PkmnView view, int pkmnnumber){
        Services.get().getPkmnService().getPokemonForm(pkmnnumber).enqueue(new Callback<PokemonForm>() {
            @Override
            public void onResponse(Call<PokemonForm> call, Response<PokemonForm> response) {
                PokemonForm pkmnf = response.body();
                String urlstring = pkmnf.getSprites().getFrontDefault();

                final URL url;
                try {
                    url = new URL(urlstring);
                    final HttpURLConnection connection = (HttpURLConnection) url
                            .openConnection();
                    connection.setRequestProperty(
                            "User-Agent",
                            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) " +
                                    "AppleWebKit/537.31 (KHTML, like Gecko) " +
                                    "Chrome/26.0.1410.65 " +
                                    "Safari/537.31");
                    final BufferedImage image = ImageIO.read(connection.getInputStream());
                    view.setImage(SwingFXUtils.toFXImage(image, null));
                    Platform.runLater(()->view.setName(pkmnf.getName()));
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<PokemonForm> call, Throwable throwable) {
                System.out.println(throwable.getMessage());
            }
        });
    }


}
