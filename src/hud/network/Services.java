package hud.network;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.concurrent.TimeUnit;

/**
 * @author Neal Beeken on Jul 18, 2016.
 */
public class Services {

    private PkmnService pkmnService;

    private Services() {

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(PkmnService.API)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        this.pkmnService = retrofit.create(PkmnService.class);
    }

    private static Services s;
    public static Services get(){
        if(s==null) s = new Services();
        return s;
    }

    public PkmnService getPkmnService() {
        return pkmnService;
    }

    public void setPkmnService(PkmnService pkmnService) {
        this.pkmnService = pkmnService;
    }

}
