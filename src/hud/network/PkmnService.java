package hud.network;

import hud.model.PokemonForm;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 *
 * @author Neal Beeken on Jul 18, 2016.
 */
public interface PkmnService {

    String API = "http://pokeapi.co/api/v2/";

    @GET("pokemon-form/{id}/")
    Call<PokemonForm> getPokemonForm(@Path("id") int id);

}
