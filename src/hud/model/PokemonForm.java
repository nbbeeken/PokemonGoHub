
package hud.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class PokemonForm {

    @SerializedName("is_battle_only")
    @Expose
    private Boolean isBattleOnly;
    @SerializedName("sprites")
    @Expose
    private Sprites sprites;
    @SerializedName("version_group")
    @Expose
    private VersionGroup versionGroup;
    @SerializedName("form_order")
    @Expose
    private Integer formOrder;
    @SerializedName("is_mega")
    @Expose
    private Boolean isMega;
    @SerializedName("form_names")
    @Expose
    private List<Object> formNames = new ArrayList<Object>();
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("is_default")
    @Expose
    private Boolean isDefault;
    @SerializedName("names")
    @Expose
    private List<Object> names = new ArrayList<Object>();
    @SerializedName("form_name")
    @Expose
    private String formName;
    @SerializedName("pokemon")
    @Expose
    private Pokemon pokemon;
    @SerializedName("order")
    @Expose
    private Integer order;
    @SerializedName("name")
    @Expose
    private String name;

    /**
     *
     * @return
     *     The isBattleOnly
     */
    public Boolean getIsBattleOnly() {
        return isBattleOnly;
    }

    /**
     *
     * @param isBattleOnly
     *     The is_battle_only
     */
    public void setIsBattleOnly(Boolean isBattleOnly) {
        this.isBattleOnly = isBattleOnly;
    }

    /**
     *
     * @return
     *     The sprites
     */
    public Sprites getSprites() {
        return sprites;
    }

    /**
     *
     * @param sprites
     *     The sprites
     */
    public void setSprites(Sprites sprites) {
        this.sprites = sprites;
    }

    /**
     *
     * @return
     *     The versionGroup
     */
    public VersionGroup getVersionGroup() {
        return versionGroup;
    }

    /**
     *
     * @param versionGroup
     *     The version_group
     */
    public void setVersionGroup(VersionGroup versionGroup) {
        this.versionGroup = versionGroup;
    }

    /**
     *
     * @return
     *     The formOrder
     */
    public Integer getFormOrder() {
        return formOrder;
    }

    /**
     *
     * @param formOrder
     *     The form_order
     */
    public void setFormOrder(Integer formOrder) {
        this.formOrder = formOrder;
    }

    /**
     *
     * @return
     *     The isMega
     */
    public Boolean getIsMega() {
        return isMega;
    }

    /**
     *
     * @param isMega
     *     The is_mega
     */
    public void setIsMega(Boolean isMega) {
        this.isMega = isMega;
    }

    /**
     *
     * @return
     *     The formNames
     */
    public List<Object> getFormNames() {
        return formNames;
    }

    /**
     *
     * @param formNames
     *     The form_names
     */
    public void setFormNames(List<Object> formNames) {
        this.formNames = formNames;
    }

    /**
     *
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     *     The isDefault
     */
    public Boolean getIsDefault() {
        return isDefault;
    }

    /**
     *
     * @param isDefault
     *     The is_default
     */
    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    /**
     *
     * @return
     *     The names
     */
    public List<Object> getNames() {
        return names;
    }

    /**
     *
     * @param names
     *     The names
     */
    public void setNames(List<Object> names) {
        this.names = names;
    }

    /**
     *
     * @return
     *     The formName
     */
    public String getFormName() {
        return formName;
    }

    /**
     *
     * @param formName
     *     The form_name
     */
    public void setFormName(String formName) {
        this.formName = formName;
    }

    /**
     *
     * @return
     *     The pokemon
     */
    public Pokemon getPokemon() {
        return pokemon;
    }

    /**
     *
     * @param pokemon
     *     The pokemon
     */
    public void setPokemon(Pokemon pokemon) {
        this.pokemon = pokemon;
    }

    /**
     *
     * @return
     *     The order
     */
    public Integer getOrder() {
        return order;
    }

    /**
     *
     * @param order
     *     The order
     */
    public void setOrder(Integer order) {
        this.order = order;
    }

    /**
     *
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

}
