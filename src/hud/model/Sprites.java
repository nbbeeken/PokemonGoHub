
package hud.model;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Sprites {

    @SerializedName("front_default")
    @Expose
    private String frontDefault;
    @SerializedName("back_default")
    @Expose
    private String backDefault;
    @SerializedName("front_shiny")
    @Expose
    private String frontShiny;
    @SerializedName("back_shiny")
    @Expose
    private String backShiny;

    /**
     * 
     * @return
     *     The frontDefault
     */
    public String getFrontDefault() {
        return frontDefault;
    }

    /**
     * 
     * @param frontDefault
     *     The front_default
     */
    public void setFrontDefault(String frontDefault) {
        this.frontDefault = frontDefault;
    }

    /**
     * 
     * @return
     *     The backDefault
     */
    public String getBackDefault() {
        return backDefault;
    }

    /**
     * 
     * @param backDefault
     *     The back_default
     */
    public void setBackDefault(String backDefault) {
        this.backDefault = backDefault;
    }

    /**
     * 
     * @return
     *     The frontShiny
     */
    public String getFrontShiny() {
        return frontShiny;
    }

    /**
     * 
     * @param frontShiny
     *     The front_shiny
     */
    public void setFrontShiny(String frontShiny) {
        this.frontShiny = frontShiny;
    }

    /**
     * 
     * @return
     *     The backShiny
     */
    public String getBackShiny() {
        return backShiny;
    }

    /**
     * 
     * @param backShiny
     *     The back_shiny
     */
    public void setBackShiny(String backShiny) {
        this.backShiny = backShiny;
    }

}
