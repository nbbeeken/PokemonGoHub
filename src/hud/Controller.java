package hud;

import com.sun.istack.internal.Nullable;
import javafx.animation.Animation;
import javafx.animation.PathTransition;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

class Controller {

    private Pane root;
    private final Delta dragDelta;
    private Stage stage;

    Controller(Pane root, Stage stage) {
        this.root = root;
        this.stage = stage;
        dragDelta = new Delta();
    }

    void handleKeyPress(KeyEvent event){
        if (event.getCode().equals(KeyCode.ESCAPE)) {
            stage.close();
            quit(null);
        }
        Text text = new Text(event.getCharacter());
        text.setFont(Font.font(22));
        root.getChildren().add(text);

        Rectangle circle = new Rectangle(100,100);
        circle.setTranslateX(300);
        circle.setTranslateY(300);

        PathTransition pt = new PathTransition(Duration.seconds(2), circle, text);
        //pt.setAutoReverse(true);
        pt.setCycleCount(Animation.INDEFINITE);
        pt.play();
    }

    void quit(@Nullable MouseEvent mouseEvent) {
        System.exit(0);
    }

    void moveWindowPressed(MouseEvent mouseEvent){
        dragDelta.x = stage.getX() - mouseEvent.getScreenX();
        dragDelta.y = stage.getY() - mouseEvent.getScreenY();
    }

    void moveWindowDragged(MouseEvent mouseEvent){
        stage.setX(mouseEvent.getScreenX() + dragDelta.x);
        stage.setY(mouseEvent.getScreenY() + dragDelta.y);
    }

    void minimize(MouseEvent mouseEvent) {
        stage.setIconified(true);
    }

}

class Delta { double x, y; }
