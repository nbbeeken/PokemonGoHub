package resources;

import java.io.InputStream;

/**
 * Created by neal on 018, 7/18/2016.
 */
public class Res {
    public static InputStream getRes(String resname){
        return Res.class.getResourceAsStream(resname);
    }
    public static String getResPath(String resname){
        String s = "";
        try{
            s = "file://" + Res.class.getResource(resname).getPath();
        }catch (NullPointerException ex){
            System.err.println("BAD FILE NAME");
        }
        return s;
    }
}
